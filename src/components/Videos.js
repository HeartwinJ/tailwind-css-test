import React, { Component } from 'react'
import VideoItem from './VideoItem'

export default class Videos extends Component {
  render() {
    return (
      <div className="bg-gray-600 w-full p-4">
        <div className="grid grid-cols-4">
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=1" title="5 Amazing Features coming to your iPhone" channel="Matt bonez" views="123k views" time="19 hours ago" /></div>
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=2" title="Trip to Himalayas" channel="Tick Lick" views="7B views" time="1 min ago" /></div>
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=3" title="Hyundai Avacazar 2.2L Petrol" channel="Fast and Loaded" views="132k views" time="1 week ago" /></div>
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=4" title="Introducing Team Iron Man" channel="Ctrl Alt W" views="703k views" time="1 month ago" /></div>
        </div>
        <div className="grid grid-cols-4">
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=5" title="Kill your excuses" channel="Ben Lee Scott" views="0 views" time="12 years ago" /></div>
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=6" title="Can a ball spin anymore?" channel="Xneon" views="2.9M" time="7 months ago" /></div>
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=7" title="Tonight show search party with Miley Cyrus" channel="The Tonight show" views="148k" time="1 day ago" /></div>
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=8" title="Sony Experia 369" channel="MKBHD" views="2.9M" time="1 day ago" /></div>
        </div>
        <div className="grid grid-cols-4">
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=9" title="Michael Jackson vs Melvyn Victor" channel="Gen Reviews" views="2.5B" time="1 hour ago" /></div>
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=10" title="Christiano Ronaldo: Tested to the Limit ft. Mashooq" channel="CR7 Official" views="17M" time="2 years ago" /></div>
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=11" title="Norway 4k Scenic Relaxation" channel="Scenic Relaxation" views="2.8M" time="2 years ago" /></div>
          <div className="m-3"><VideoItem thumbUrl="https://picsum.photos/1280/720?random=12" title="Orange Review ft. Gokulan TK" channel="Gen Reviews" views="2B" time="1 min ago" /></div>
        </div>
      </div>
    )
  }
}
