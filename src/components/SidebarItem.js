import React, { Component } from 'react'

export default class SidebarItem extends Component {
  render() {
    return (
      <div className="flex items-center">
        <div className="flex items-center justify-items-start">
          {this.props.children}
        </div>
        <span className="ml-4 font-semibold">{this.props.title}</span>
      </div>
    )
  }
}
