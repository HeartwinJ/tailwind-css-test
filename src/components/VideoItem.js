import { UserCircleIcon } from '@heroicons/react/outline'
import React, { Component } from 'react'

export default class VideoItem extends Component {
  render() {
    const { thumbUrl, title, channel, views, time } = this.props;
    return (
      <div className="flex flex-col h-full text-white">
        <div className="bg-gray-400">
          <img className="w-100 h-40 object-cover object-center" src={thumbUrl}></img>
        </div>
        <div className="flex p-2">
          <UserCircleIcon className="h-10" />
          <div className="ml-3">
            <div>{title}</div>
            <div>{channel}</div>
            <div><span>{views}</span> &bull; <span>{time}</span></div>
          </div>
        </div>
      </div>
    )
  }
}
