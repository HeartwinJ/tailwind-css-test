import React, { Component } from 'react'
import { MenuIcon, VideoCameraIcon, ViewGridIcon, BellIcon, BadgeCheckIcon, MicrophoneIcon, SearchIcon } from '@heroicons/react/outline'

export default class Header extends Component {
  render() {
    return (
      <div className="bg-gray-800 text-white p-4 flex items-center justify-between">
        <div className="flex">
          <MenuIcon className="icon"></MenuIcon>
          <img src="assets/yt_logo.png" className="h-6 ml-5"></img>
        </div>
        <div className="flex items-center">
          <input type="text" className="rounded-l h-8 w-96 bg-gray-900"></input>
          <button className="py-1 px-6 rounded-r bg-gray-700"><SearchIcon className="icon"></SearchIcon></button>
          <MicrophoneIcon className="ml-3 icon"></MicrophoneIcon>
        </div>
        <div className="flex space-x-6">
          <VideoCameraIcon className="icon"></VideoCameraIcon>
          <ViewGridIcon className="icon"></ViewGridIcon>
          <BellIcon className="icon"></BellIcon>
          <BadgeCheckIcon className="icon"></BadgeCheckIcon>
        </div>
      </div>
    )
  }
}
