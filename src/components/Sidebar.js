import React, { Component } from 'react'
import SidebarItem from './SidebarItem'
import { HomeIcon, GlobeIcon, CollectionIcon, ChevronDownIcon, ThumbUpIcon, ClockIcon, ViewBoardsIcon, NewspaperIcon, PhotographIcon, UserCircleIcon } from '@heroicons/react/solid'

export default class Sidebar extends Component {
  render() {
    return (
      <div className="bg-gray-700 text-white text-lg w-1/5">
        <div className="px-6 py-4 space-y-3">
          <SidebarItem title="Home"><HomeIcon className="icon" /></SidebarItem>
          <SidebarItem title="Explore"><GlobeIcon className="icon" /></SidebarItem>
          <SidebarItem title="Subscriptions"><CollectionIcon className="icon" /></SidebarItem>
        </div>
        <hr></hr>
        <div className="px-6 py-4 space-y-3">
          <SidebarItem title="Library"><ViewBoardsIcon className="icon" /></SidebarItem>
          <SidebarItem title="History"><NewspaperIcon className="icon" /></SidebarItem>
          <SidebarItem title="Your Videos"><PhotographIcon className="icon" /></SidebarItem>
          <SidebarItem title="Watched Videos"><ClockIcon className="icon" /></SidebarItem>
          <SidebarItem title="Liked Videos"><ThumbUpIcon className="icon" /></SidebarItem>
          <SidebarItem title="Show More"><ChevronDownIcon className="icon" /></SidebarItem>
        </div>
        <hr></hr>
        <div className="px-6 py-4 space-y-3">
          <span>SUBSCRIPTIONS</span>
          <SidebarItem title="Alec Steele"><UserCircleIcon className="icon" /></SidebarItem>
          <SidebarItem title="Alexander Babu"><UserCircleIcon className="icon" /></SidebarItem>
          <SidebarItem title="Ali Gatie"><UserCircleIcon className="icon" /></SidebarItem>
          <SidebarItem title="Anime Sensei"><UserCircleIcon className="icon" /></SidebarItem>
          <SidebarItem title="Anwar Jibawi"><UserCircleIcon className="icon" /></SidebarItem>
          <SidebarItem title="Ariana Grande"><UserCircleIcon className="icon" /></SidebarItem>
        </div>
      </div>
    )
  }
}
