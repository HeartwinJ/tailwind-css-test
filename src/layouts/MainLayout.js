import React, { Component } from 'react'
import Header from '../components/Header'
import Sidebar from '../components/Sidebar'
import Videos from '../components/Videos'

export default class MainLayout extends Component {
  render() {
    return (
      <div className="h-full flex flex-col">
        <Header></Header>
        <div className="flex flex-grow">
          <Sidebar></Sidebar>
          <Videos></Videos>
        </div>
      </div>
    )
  }
}
